<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $category = new Category();
        $category->setlabel('Informatique');
        $manager->persist($category);

        $category = new Category();
        $category->setlabel('Electroménager');
        $manager->persist($category);

        $category = new Category();
        $category->setlabel('video');
        $manager->persist($category);

        $manager->flush();
    }
}

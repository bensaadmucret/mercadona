<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Faker\Generator;
use Faker\Factory;
use App\Entity\Product;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class ProductFixtures extends Fixture
{
    /**
     * @var Generator
     */
    private Generator $faker;

    public function __construct()
    {
        $this->faker = Factory::create('fr_FR');
    }
    public function load(ObjectManager $manager): void
    {
        // create 20 products! Bam!
        for ($i = 0; $i < 20; $i++) {

            $category = new Category();
            $product = new Product();
            $product->setLabel('Produit' .' ' .  $i);
            $product->setDescription($this->faker->paragraph);
            $product->setPrice(mt_rand(10, 100));
            $product->setPicture('macbook-air-20-de-remise-sur-le-pc-portable-apple-a-la-fnac.jpeg');


            $category->setLabel($this->faker->word);
            $manager->persist($category);

            $product->setProductCategory($category);
            $manager->persist($product);

        }

        $manager->flush();
    }
}

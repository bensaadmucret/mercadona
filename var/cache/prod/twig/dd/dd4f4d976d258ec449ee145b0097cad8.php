<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_d33d5ae0bd87eb3f140f6a7c55217d6b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head lang=\"fr\">
        <meta charset=\"UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <meta name=\"description\" content=\"Mercadona, le supermarché en ligne\" />
        <meta name=\"keywords\" content=\"Mercadona, supermarché, courses, livraison\" />
        <meta name=\"author\" content=\"Mercadona\" />

        <link rel=\"icon\" href=\"data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 128 128%22><text y=%221.2em%22 font-size=%2296%22>⚫️</text></svg>\">
        ";
        // line 13
        echo "        ";
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 17
        echo "
        ";
        // line 18
        $this->displayBlock('javascripts', $context, $blocks);
        // line 21
        echo "    </head>
    <body class=\"body-bg min-h-screen   pb-6 px-2 md:px-0\" style=\"font-family:'Lato',sans-serif;\">
    
    <header>

<nav class=\"bg-white border-gray-200 dark:bg-gray-900\">
  <div class=\"max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4\">
    <a href=\"";
        // line 28
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_home");
        echo "\" class=\"flex items-center\">
        <span class=\"self-center text-2xl font-semibold whitespace-nowrap dark:text-white\">Mercadona</span>
    </a>
    <button data-collapse-toggle=\"navbar-default\" type=\"button\" class=\"inline-flex items-center p-2 ml-3 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600\" aria-controls=\"navbar-default\" aria-expanded=\"false\">
      <span class=\"sr-only\">Open main menu</span>
      <svg class=\"w-6 h-6\" aria-hidden=\"true\" fill=\"currentColor\" viewBox=\"0 0 20 20\" xmlns=\"http://www.w3.org/2000/svg\"><path fill-rule=\"evenodd\" d=\"M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z\" clip-rule=\"evenodd\"></path></svg>
    </button>
    <div class=\"hidden w-full md:block md:w-auto\" id=\"navbar-default\">
      <ul class=\"font-medium flex flex-col p-4 md:p-0 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 md:mt-0 md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700\">
        <li>
          <a href=\"";
        // line 38
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_home");
        echo "\" class=\"block py-2 pl-3 pr-4 text-black rounded md:bg-transparent md:text-blue-700 md:p-0 dark:text-white md:dark:text-blue-500\" aria-current=\"page\">Accueil</a>
        </li>
        <li>
          <a href=\"";
        // line 41
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_product_index");
        echo "\" class=\"block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent\">Catalogue</a>
        </li>
        ";
        // line 43
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            echo " 
        <li>
          <a href=\"";
            // line 45
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin");
            echo "\" class=\"block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent\">Admin</a>
        </li> 
        ";
        }
        // line 47
        echo "        
        ";
        // line 48
        if (twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 48)) {
            // line 49
            echo "           <li>
          <a href=\"";
            // line 50
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_logout");
            echo "\" class=\"block py-2 pl-3 pr-4 text-black rounded md:bg-transparent md:text-blue-700 md:p-0 dark:text-white md:dark:text-blue-500\" aria-current=\"page\">Déconnexion</a>
        </li>
        ";
        } else {
            // line 53
            echo "        <li>
          <a href=\"";
            // line 54
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_login");
            echo "\" class=\"block py-2 pl-3 pr-4 text-black rounded md:bg-transparent md:text-blue-700 md:p-0 dark:text-white md:dark:text-blue-500\" aria-current=\"page\">Connexion</a>
        </li>
        ";
        }
        // line 57
        echo "      </ul>
    </div>
  </div>
</nav>
</header>

    ";
        // line 63
        $this->displayBlock('body', $context, $blocks);
        // line 65
        echo "    <script src=\"https://unpkg.com/@themesberg/flowbite@1.1.1/dist/flowbite.bundle.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/flowbite.min.js\"></script>

    <footer class=\"dark:bg-gray-900\">
        <div class=\"mx-auto w-full max-w-screen-xl p-4 py-6 lg:py-8\">
            <div class=\"md:flex md:justify-between\">
                <div class=\"mb-6 md:mb-0\">
                    <a class=\"flex items-center\">
                        <span class=\"self-center text-2xl font-semibold whitespace-nowrap dark:text-white\">Mercadona</span>
                    </a>
                    <p class=\"mt-2 text-gray-500 dark:text-gray-400\">Mercadona des produits aux meilleurs prix !.</p>
                </div>
                <div class=\"grid grid-cols-2 gap-8 sm:gap-6 sm:grid-cols-3\">
                    <div>
                        <h2 class=\"mb-6 text-sm font-semibold text-gray-900 uppercase dark:text-white\">Resources</h2>
                        <ul class=\"text-gray-600 dark:text-gray-400 font-medium\">
                            <li class=\"mb-4\">
                                <a class=\"hover:underline\">Mercadona</a>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <h2 class=\"mb-6 text-sm font-semibold text-gray-900 uppercase dark:text-white\">Legal</h2>
                        <ul class=\"text-gray-600 dark:text-gray-400 font-medium\">
                            <li class=\"mb-4\">
                                <a href=\"#\" class=\"hover:underline\">Privacy Policy</a>
                            </li>
                            <li>
                                <a href=\"#\" class=\"hover:underline\">Terms &amp; Conditions</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <hr class=\"my-6 border-black-200 sm:mx-auto dark:border-gray-700 lg:my-8\" />
            <div class=\"sm:flex sm:items-center sm:justify-between\">
          <span class=\"text-sm text-gray-500 sm:text-center dark:text-gray-400\">© 2023 <a  class=\"hover:underline\">Mercadona™</a>. All Rights Reserved.
          </span>
        </div>
    </footer>
    </body>
</html>
";
    }

    // line 6
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Welcome!";
    }

    // line 13
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 14
        echo "            <link href=\"https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap\" rel=\"stylesheet\">
            ";
        // line 15
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("app");
        echo "
        ";
    }

    // line 18
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 19
        echo "            ";
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("app");
        echo "
        ";
    }

    // line 63
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 64
        echo "    ";
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  223 => 64,  219 => 63,  212 => 19,  208 => 18,  202 => 15,  199 => 14,  195 => 13,  188 => 6,  142 => 65,  140 => 63,  132 => 57,  126 => 54,  123 => 53,  117 => 50,  114 => 49,  112 => 48,  109 => 47,  103 => 45,  98 => 43,  93 => 41,  87 => 38,  74 => 28,  65 => 21,  63 => 18,  60 => 17,  57 => 13,  48 => 6,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "base.html.twig", "/Users/ben/Downloads/Projet-Mercadona-main/templates/base.html.twig");
    }
}

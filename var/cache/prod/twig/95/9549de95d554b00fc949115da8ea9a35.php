<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/my-dashboard.html.twig */
class __TwigTemplate_b7c26790f5cb4bd6b53cfdbe3b489a8f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\">
    <title>My Dashboard</title>
    <!-- Ajoutez ici vos liens vers les fichiers CSS -->
    <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/easyadmin@latest/easyadmin.min.css\">
</head>
<body>
<header>
    <!-- Ajoutez ici le contenu de votre en-tête -->
    <h1>Mon Tableau de bord EasyAdmin</h1>
</header>
<nav>
    <!-- Ajoutez ici votre menu de navigation -->
    <ul>
        <li><a href=\"";
        // line 17
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_category");
        echo "\">Accueil</a></li>

    </ul>
</nav>
<main>
    <!-- Ajoutez ici le contenu principal de votre tableau de bord -->
    <h2>Bienvenue sur votre tableau de bord EasyAdmin</h2>
    <!-- Inclure ici le rendu de EasyAdmin -->

</main>
<footer>
    <!-- Ajoutez ici le contenu de votre pied de page -->
    <p>© 2023 Mon Entreprise. Tous droits réservés.</p>
</footer>
<!-- Ajoutez ici vos liens vers les fichiers JavaScript -->
<script src=\"https://cdn.jsdelivr.net/npm/easyadmin@latest/easyadmin.min.js\"></script>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "admin/my-dashboard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 17,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/my-dashboard.html.twig", "/Users/ben/Downloads/Projet-Mercadona-main/templates/admin/my-dashboard.html.twig");
    }
}

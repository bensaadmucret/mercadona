<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* product/index.html.twig */
class __TwigTemplate_43e6075cbb53a268d79f8446993c3e54 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "product/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Product index";
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <div class=\"flex flex-col md:flex-row\">
        <div class=\"w-full md:w-1/4 p-2\">
            <div class=\"p-4\">
                <div class=\"first:font-normal\">
                    <button class=\"bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded m-3\">
                        <a href=\"";
        // line 11
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_product_index");
        echo "\">Toutes les catégories</a>
                    </button>
                </div>
                <select class=\"block w-full p-2 border border-gray-300 rounded-md dark:bg-gray-200 dark:text-gray-800\" onchange=\"location = this.value;\">
                    <option value=\"";
        // line 15
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_product_index");
        echo "\">Filtres par catégories</option>
                    ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 17
            echo "                        <option value=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_categorie_index", ["id" => twig_get_attribute($this->env, $this->source, $context["category"], "id", [], "any", false, false, false, 17)]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "label", [], "any", false, false, false, 17), "html", null, true);
            echo "</option>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "                </select>
            </div>
        </div>
        <div class=\"w-full md:w-3/4 p-3\">
            <!-- Contenu des produits -->
            <div class=\"container mx-auto px-4 \">
                <div class=\"mt-6 grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-2 gap-8 dark:text-gray-400 xl:gap-x-8\">
                    ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 27
            echo "                        <div class=\"group relative gap-8 drop-shadow-md  bg-white shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)] dark:bg-neutral-700\">
                            <div class=\"min-h-80 aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-md bg-gray-200 lg:aspect-none group-hover:opacity-75 lg:h-80\">
                                <img src=\"";
            // line 29
            echo twig_escape_filter($this->env, ($context["app_pictures_dir"] ?? null), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "picture", [], "any", false, false, false, 29), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "label", [], "any", false, false, false, 29), "html", null, true);
            echo "\" class=\"h-full w-full object-cover object-center lg:h-full lg:w-full\">
                            </div>
                            <div class=\"mt-4 flex justify-between p-4 \">
                                <div>

                                </div>
                                ";
            // line 35
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "productPromotion", [], "any", false, true, false, 35), "promotionPercentage", [], "any", true, true, false, 35)) {
                // line 36
                echo "                                    <h3 class=\"text-md text-gray-900 font-bold p-4 \">
                                            ";
                // line 37
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "label", [], "any", false, false, false, 37), "html", null, true);
                echo "
                                    </h3>
                                    ";
                // line 39
                $context["promoPrice"] = ((twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 39) * twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "productPromotion", [], "any", false, false, false, 39), "promotionPercentage", [], "any", false, false, false, 39)) / 100);
                // line 40
                echo "                                    <p class=\"m-2 text-md text-grey-900 font-bold line-through\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 40), "html", null, true);
                echo " €</p>
                                <span class=\"inline-flex items-center bg-red-100 text-red-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded-full dark:bg-red-900 dark:text-red-300\">
                                    <p class=\"m-2 text-md text-red-500 font-bold\">";
                // line 42
                echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 42) - ($context["promoPrice"] ?? null)), "html", null, true);
                echo " €</p>
                                </span>
                                ";
            } else {
                // line 45
                echo "                                <h3 class=\"text-md text-gray-900 font-bold p-4 \">
                                        ";
                // line 46
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "label", [], "any", false, false, false, 46), "html", null, true);
                echo "
                                </h3>
                                <span class=\"inline-flex items-center bg-blue-100 text-blue-500 text-xs font-medium mr-2 px-2.5 py-0.5 rounded-full dark:bg-red-900 dark:text-red-300\">
                                    <p class=\"m-2 text-md text-blue-500\">";
                // line 49
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 49), "html", null, true);
                echo " €</p>
                                </span>
                                ";
            }
            // line 52
            echo "
                            </div>
                            <div class=\"block max-w-sm p-6 \">
                                <p class=\"font-normal text-gray-700 dark:text-gray-400\">";
            // line 55
            echo twig_get_attribute($this->env, $this->source, $context["product"], "description", [], "any", false, false, false, 55);
            echo "</p>
                            </div>


                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 61
        echo "                </div>
            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "product/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  177 => 61,  165 => 55,  160 => 52,  154 => 49,  148 => 46,  145 => 45,  139 => 42,  133 => 40,  131 => 39,  126 => 37,  123 => 36,  121 => 35,  108 => 29,  104 => 27,  100 => 26,  91 => 19,  80 => 17,  76 => 16,  72 => 15,  65 => 11,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "product/index.html.twig", "/Users/ben/Downloads/Projet-Mercadona-main/templates/product/index.html.twig");
    }
}

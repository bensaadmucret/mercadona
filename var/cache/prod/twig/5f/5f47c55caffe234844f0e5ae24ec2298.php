<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/index.html.twig */
class __TwigTemplate_58b9617af13a9ef02441bf83c366f938 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "home/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Mercadona";
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
  <div class=\"relative overflow-hidden bg-cover bg-no-repeat p-12 text-center\" style=\"height: 900px; max-height: 700px; background-image: url('https://tecdn.b-cdn.net/img/Photos/Slides/img%20(15).jpg'); background-position: center; background-size: cover;\">
    <div class=\"absolute bottom-0 left-0 right-0 top-0 h-full w-full overflow-hidden bg-fixed\" style=\"background-color: rgba(0, 0, 0, 0.6)\">
      <div class=\"flex h-full items-center justify-center\">
        <div class=\"text-white\">
          <h2 class=\"mb-4 text-4xl font-semibold\">Bienvenue chez Mercadona</h2>
          <p class=\"mb-6 text-lg\">Découvrez notre large sélection de produits de qualité.</p>
          <a href=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_product_index");
        echo "\" class=\"px-6 py-3 bg-blue-500 text-white font-semibold rounded-lg\">Voir les produits</a>
        </div>
      </div>
    </div>
  </div>

";
    }

    public function getTemplateName()
    {
        return "home/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 13,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "home/index.html.twig", "/Users/ben/Downloads/Projet-Mercadona-main/templates/home/index.html.twig");
    }
}

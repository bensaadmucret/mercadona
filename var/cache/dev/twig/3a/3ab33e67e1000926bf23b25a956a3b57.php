<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_1e74f8514c7444b8b95e6348046546bc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head lang=\"fr\">
        <meta charset=\"UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <meta name=\"description\" content=\"Mercadona, le supermarché en ligne\" />
        <meta name=\"keywords\" content=\"Mercadona, supermarché, courses, livraison\" />
        <meta name=\"author\" content=\"Mercadona\" />

        <link rel=\"icon\" href=\"data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 128 128%22><text y=%221.2em%22 font-size=%2296%22>⚫️</text></svg>\">
        ";
        // line 13
        echo "        ";
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 17
        echo "
        ";
        // line 18
        $this->displayBlock('javascripts', $context, $blocks);
        // line 21
        echo "    </head>
    <body class=\"body-bg min-h-screen   pb-6 px-2 md:px-0\" style=\"font-family:'Lato',sans-serif;\">
    
    <header>

<nav class=\"bg-white border-gray-200 dark:bg-gray-900\">
  <div class=\"max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4\">
    <a href=\"";
        // line 28
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_home");
        echo "\" class=\"flex items-center\">
        <span class=\"self-center text-2xl font-semibold whitespace-nowrap dark:text-white\">Mercadona</span>
    </a>
    <button data-collapse-toggle=\"navbar-default\" type=\"button\" class=\"inline-flex items-center p-2 ml-3 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600\" aria-controls=\"navbar-default\" aria-expanded=\"false\">
      <span class=\"sr-only\">Open main menu</span>
      <svg class=\"w-6 h-6\" aria-hidden=\"true\" fill=\"currentColor\" viewBox=\"0 0 20 20\" xmlns=\"http://www.w3.org/2000/svg\"><path fill-rule=\"evenodd\" d=\"M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z\" clip-rule=\"evenodd\"></path></svg>
    </button>
    <div class=\"hidden w-full md:block md:w-auto\" id=\"navbar-default\">
      <ul class=\"font-medium flex flex-col p-4 md:p-0 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 md:mt-0 md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700\">
        <li>
          <a href=\"";
        // line 38
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_home");
        echo "\" class=\"block py-2 pl-3 pr-4 text-black rounded md:bg-transparent md:text-blue-700 md:p-0 dark:text-white md:dark:text-blue-500\" aria-current=\"page\">Accueil</a>
        </li>
        <li>
          <a href=\"";
        // line 41
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_product_index");
        echo "\" class=\"block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent\">Catalogue</a>
        </li>
        ";
        // line 43
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            echo " 
        <li>
          <a href=\"";
            // line 45
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin");
            echo "\" class=\"block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent\">Admin</a>
        </li> 
        ";
        }
        // line 47
        echo "        
        ";
        // line 48
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 48, $this->source); })()), "user", [], "any", false, false, false, 48)) {
            // line 49
            echo "           <li>
          <a href=\"";
            // line 50
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_logout");
            echo "\" class=\"block py-2 pl-3 pr-4 text-black rounded md:bg-transparent md:text-blue-700 md:p-0 dark:text-white md:dark:text-blue-500\" aria-current=\"page\">Déconnexion</a>
        </li>
        ";
        } else {
            // line 53
            echo "        <li>
          <a href=\"";
            // line 54
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_login");
            echo "\" class=\"block py-2 pl-3 pr-4 text-black rounded md:bg-transparent md:text-blue-700 md:p-0 dark:text-white md:dark:text-blue-500\" aria-current=\"page\">Connexion</a>
        </li>
        ";
        }
        // line 57
        echo "      </ul>
    </div>
  </div>
</nav>
</header>

    ";
        // line 63
        $this->displayBlock('body', $context, $blocks);
        // line 65
        echo "    <script src=\"https://unpkg.com/@themesberg/flowbite@1.1.1/dist/flowbite.bundle.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/flowbite.min.js\"></script>

    <footer class=\"dark:bg-gray-900\">
        <div class=\"mx-auto w-full max-w-screen-xl p-4 py-6 lg:py-8\">
            <div class=\"md:flex md:justify-between\">
                <div class=\"mb-6 md:mb-0\">
                    <a class=\"flex items-center\">
                        <span class=\"self-center text-2xl font-semibold whitespace-nowrap dark:text-white\">Mercadona</span>
                    </a>
                    <p class=\"mt-2 text-gray-500 dark:text-gray-400\">Mercadona des produits aux meilleurs prix !.</p>
                </div>
                <div class=\"grid grid-cols-2 gap-8 sm:gap-6 sm:grid-cols-3\">
                    <div>
                        <h2 class=\"mb-6 text-sm font-semibold text-gray-900 uppercase dark:text-white\">Resources</h2>
                        <ul class=\"text-gray-600 dark:text-gray-400 font-medium\">
                            <li class=\"mb-4\">
                                <a class=\"hover:underline\">Mercadona</a>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <h2 class=\"mb-6 text-sm font-semibold text-gray-900 uppercase dark:text-white\">Legal</h2>
                        <ul class=\"text-gray-600 dark:text-gray-400 font-medium\">
                            <li class=\"mb-4\">
                                <a href=\"#\" class=\"hover:underline\">Privacy Policy</a>
                            </li>
                            <li>
                                <a href=\"#\" class=\"hover:underline\">Terms &amp; Conditions</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <hr class=\"my-6 border-black-200 sm:mx-auto dark:border-gray-700 lg:my-8\" />
            <div class=\"sm:flex sm:items-center sm:justify-between\">
          <span class=\"text-sm text-gray-500 sm:text-center dark:text-gray-400\">© 2023 <a  class=\"hover:underline\">Mercadona™</a>. All Rights Reserved.
          </span>
        </div>
    </footer>
    </body>
</html>
";
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 6
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 13
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 14
        echo "            <link href=\"https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap\" rel=\"stylesheet\">
            ";
        // line 15
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("app");
        echo "
        ";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 18
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 19
        echo "            ";
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("app");
        echo "
        ";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 63
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 64
        echo "    ";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  277 => 64,  267 => 63,  254 => 19,  244 => 18,  232 => 15,  229 => 14,  219 => 13,  200 => 6,  148 => 65,  146 => 63,  138 => 57,  132 => 54,  129 => 53,  123 => 50,  120 => 49,  118 => 48,  115 => 47,  109 => 45,  104 => 43,  99 => 41,  93 => 38,  80 => 28,  71 => 21,  69 => 18,  66 => 17,  63 => 13,  54 => 6,  47 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
    <head lang=\"fr\">
        <meta charset=\"UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        <meta name=\"description\" content=\"Mercadona, le supermarché en ligne\" />
        <meta name=\"keywords\" content=\"Mercadona, supermarché, courses, livraison\" />
        <meta name=\"author\" content=\"Mercadona\" />

        <link rel=\"icon\" href=\"data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 128 128%22><text y=%221.2em%22 font-size=%2296%22>⚫️</text></svg>\">
        {# Run `composer require symfony/webpack-encore-bundle` to start using Symfony UX #}
        {% block stylesheets %}
            <link href=\"https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap\" rel=\"stylesheet\">
            {{ encore_entry_link_tags('app') }}
        {% endblock %}

        {% block javascripts %}
            {{ encore_entry_script_tags('app') }}
        {% endblock %}
    </head>
    <body class=\"body-bg min-h-screen   pb-6 px-2 md:px-0\" style=\"font-family:'Lato',sans-serif;\">
    
    <header>

<nav class=\"bg-white border-gray-200 dark:bg-gray-900\">
  <div class=\"max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4\">
    <a href=\"{{ path('app_home') }}\" class=\"flex items-center\">
        <span class=\"self-center text-2xl font-semibold whitespace-nowrap dark:text-white\">Mercadona</span>
    </a>
    <button data-collapse-toggle=\"navbar-default\" type=\"button\" class=\"inline-flex items-center p-2 ml-3 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600\" aria-controls=\"navbar-default\" aria-expanded=\"false\">
      <span class=\"sr-only\">Open main menu</span>
      <svg class=\"w-6 h-6\" aria-hidden=\"true\" fill=\"currentColor\" viewBox=\"0 0 20 20\" xmlns=\"http://www.w3.org/2000/svg\"><path fill-rule=\"evenodd\" d=\"M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z\" clip-rule=\"evenodd\"></path></svg>
    </button>
    <div class=\"hidden w-full md:block md:w-auto\" id=\"navbar-default\">
      <ul class=\"font-medium flex flex-col p-4 md:p-0 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 md:mt-0 md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700\">
        <li>
          <a href=\"{{ path('app_home') }}\" class=\"block py-2 pl-3 pr-4 text-black rounded md:bg-transparent md:text-blue-700 md:p-0 dark:text-white md:dark:text-blue-500\" aria-current=\"page\">Accueil</a>
        </li>
        <li>
          <a href=\"{{ path('app_product_index') }}\" class=\"block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent\">Catalogue</a>
        </li>
        {% if is_granted('ROLE_ADMIN') %} 
        <li>
          <a href=\"{{ path('admin') }}\" class=\"block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent\">Admin</a>
        </li> 
        {% endif %}        
        {% if app.user %}
           <li>
          <a href=\"{{ path('app_logout') }}\" class=\"block py-2 pl-3 pr-4 text-black rounded md:bg-transparent md:text-blue-700 md:p-0 dark:text-white md:dark:text-blue-500\" aria-current=\"page\">Déconnexion</a>
        </li>
        {% else %}
        <li>
          <a href=\"{{ path('app_login') }}\" class=\"block py-2 pl-3 pr-4 text-black rounded md:bg-transparent md:text-blue-700 md:p-0 dark:text-white md:dark:text-blue-500\" aria-current=\"page\">Connexion</a>
        </li>
        {% endif %}
      </ul>
    </div>
  </div>
</nav>
</header>

    {% block body %}
    {% endblock %}
    <script src=\"https://unpkg.com/@themesberg/flowbite@1.1.1/dist/flowbite.bundle.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/flowbite.min.js\"></script>

    <footer class=\"dark:bg-gray-900\">
        <div class=\"mx-auto w-full max-w-screen-xl p-4 py-6 lg:py-8\">
            <div class=\"md:flex md:justify-between\">
                <div class=\"mb-6 md:mb-0\">
                    <a class=\"flex items-center\">
                        <span class=\"self-center text-2xl font-semibold whitespace-nowrap dark:text-white\">Mercadona</span>
                    </a>
                    <p class=\"mt-2 text-gray-500 dark:text-gray-400\">Mercadona des produits aux meilleurs prix !.</p>
                </div>
                <div class=\"grid grid-cols-2 gap-8 sm:gap-6 sm:grid-cols-3\">
                    <div>
                        <h2 class=\"mb-6 text-sm font-semibold text-gray-900 uppercase dark:text-white\">Resources</h2>
                        <ul class=\"text-gray-600 dark:text-gray-400 font-medium\">
                            <li class=\"mb-4\">
                                <a class=\"hover:underline\">Mercadona</a>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <h2 class=\"mb-6 text-sm font-semibold text-gray-900 uppercase dark:text-white\">Legal</h2>
                        <ul class=\"text-gray-600 dark:text-gray-400 font-medium\">
                            <li class=\"mb-4\">
                                <a href=\"#\" class=\"hover:underline\">Privacy Policy</a>
                            </li>
                            <li>
                                <a href=\"#\" class=\"hover:underline\">Terms &amp; Conditions</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <hr class=\"my-6 border-black-200 sm:mx-auto dark:border-gray-700 lg:my-8\" />
            <div class=\"sm:flex sm:items-center sm:justify-between\">
          <span class=\"text-sm text-gray-500 sm:text-center dark:text-gray-400\">© 2023 <a  class=\"hover:underline\">Mercadona™</a>. All Rights Reserved.
          </span>
        </div>
    </footer>
    </body>
</html>
", "base.html.twig", "/Users/ben/Downloads/Projet-Mercadona-main/templates/base.html.twig");
    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* product/index.html.twig */
class __TwigTemplate_26b42bddb1c856785789c77b40bac76d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "product/index.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "product/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "product/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Product index";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"flex flex-col md:flex-row\">
        <div class=\"w-full md:w-1/4 p-2\">
            <div class=\"p-4\">
                <div class=\"first:font-normal\">
                    <button class=\"bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded m-3\">
                        <a href=\"";
        // line 11
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_product_index");
        echo "\">Toutes les catégories</a>
                    </button>
                </div>
                <select class=\"block w-full p-2 border border-gray-300 rounded-md dark:bg-gray-200 dark:text-gray-800\" onchange=\"location = this.value;\">
                    <option value=\"";
        // line 15
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_product_index");
        echo "\">Filtres par catégories</option>
                    ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) || array_key_exists("categories", $context) ? $context["categories"] : (function () { throw new RuntimeError('Variable "categories" does not exist.', 16, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 17
            echo "                        <option value=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_categorie_index", ["id" => twig_get_attribute($this->env, $this->source, $context["category"], "id", [], "any", false, false, false, 17)]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "label", [], "any", false, false, false, 17), "html", null, true);
            echo "</option>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "                </select>
            </div>
        </div>
        <div class=\"w-full md:w-3/4 p-3\">
            <!-- Contenu des produits -->
            <div class=\"container mx-auto px-4 \">
                <div class=\"mt-6 grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-2 gap-8 dark:text-gray-400 xl:gap-x-8\">
                    ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) || array_key_exists("products", $context) ? $context["products"] : (function () { throw new RuntimeError('Variable "products" does not exist.', 26, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 27
            echo "                        <div class=\"group relative gap-8 drop-shadow-md  bg-white shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)] dark:bg-neutral-700\">
                            <div class=\"min-h-80 aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-md bg-gray-200 lg:aspect-none group-hover:opacity-75 lg:h-80\">
                                <img src=\"";
            // line 29
            echo twig_escape_filter($this->env, (isset($context["app_pictures_dir"]) || array_key_exists("app_pictures_dir", $context) ? $context["app_pictures_dir"] : (function () { throw new RuntimeError('Variable "app_pictures_dir" does not exist.', 29, $this->source); })()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "picture", [], "any", false, false, false, 29), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "label", [], "any", false, false, false, 29), "html", null, true);
            echo "\" class=\"h-full w-full object-cover object-center lg:h-full lg:w-full\">
                            </div>
                            <div class=\"mt-4 flex justify-between p-4 \">
                                <div>

                                </div>
                                ";
            // line 35
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "productPromotion", [], "any", false, true, false, 35), "promotionPercentage", [], "any", true, true, false, 35)) {
                // line 36
                echo "                                    <h3 class=\"text-md text-gray-900 font-bold p-4 \">
                                            ";
                // line 37
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "label", [], "any", false, false, false, 37), "html", null, true);
                echo "
                                    </h3>
                                    ";
                // line 39
                $context["promoPrice"] = ((twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 39) * twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "productPromotion", [], "any", false, false, false, 39), "promotionPercentage", [], "any", false, false, false, 39)) / 100);
                // line 40
                echo "                                    <p class=\"m-2 text-md text-grey-900 font-bold line-through\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 40), "html", null, true);
                echo " €</p>
                                <span class=\"inline-flex items-center bg-red-100 text-red-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded-full dark:bg-red-900 dark:text-red-300\">
                                    <p class=\"m-2 text-md text-red-500 font-bold\">";
                // line 42
                echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 42) - (isset($context["promoPrice"]) || array_key_exists("promoPrice", $context) ? $context["promoPrice"] : (function () { throw new RuntimeError('Variable "promoPrice" does not exist.', 42, $this->source); })())), "html", null, true);
                echo " €</p>
                                </span>
                                ";
            } else {
                // line 45
                echo "                                <h3 class=\"text-md text-gray-900 font-bold p-4 \">
                                        ";
                // line 46
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "label", [], "any", false, false, false, 46), "html", null, true);
                echo "
                                </h3>
                                <span class=\"inline-flex items-center bg-blue-100 text-blue-500 text-xs font-medium mr-2 px-2.5 py-0.5 rounded-full dark:bg-red-900 dark:text-red-300\">
                                    <p class=\"m-2 text-md text-blue-500\">";
                // line 49
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 49), "html", null, true);
                echo " €</p>
                                </span>
                                ";
            }
            // line 52
            echo "
                            </div>
                            <div class=\"block max-w-sm p-6 \">
                                <p class=\"font-normal text-gray-700 dark:text-gray-400\">";
            // line 55
            echo twig_get_attribute($this->env, $this->source, $context["product"], "description", [], "any", false, false, false, 55);
            echo "</p>
                            </div>


                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 61
        echo "                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "product/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  207 => 61,  195 => 55,  190 => 52,  184 => 49,  178 => 46,  175 => 45,  169 => 42,  163 => 40,  161 => 39,  156 => 37,  153 => 36,  151 => 35,  138 => 29,  134 => 27,  130 => 26,  121 => 19,  110 => 17,  106 => 16,  102 => 15,  95 => 11,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Product index{% endblock %}

{% block body %}
    <div class=\"flex flex-col md:flex-row\">
        <div class=\"w-full md:w-1/4 p-2\">
            <div class=\"p-4\">
                <div class=\"first:font-normal\">
                    <button class=\"bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded m-3\">
                        <a href=\"{{ path('app_product_index') }}\">Toutes les catégories</a>
                    </button>
                </div>
                <select class=\"block w-full p-2 border border-gray-300 rounded-md dark:bg-gray-200 dark:text-gray-800\" onchange=\"location = this.value;\">
                    <option value=\"{{ path('app_product_index') }}\">Filtres par catégories</option>
                    {% for category in categories %}
                        <option value=\"{{ path('app_categorie_index', { 'id': category.id }) }}\">{{ category.label }}</option>
                    {% endfor %}
                </select>
            </div>
        </div>
        <div class=\"w-full md:w-3/4 p-3\">
            <!-- Contenu des produits -->
            <div class=\"container mx-auto px-4 \">
                <div class=\"mt-6 grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-2 gap-8 dark:text-gray-400 xl:gap-x-8\">
                    {% for product in products %}
                        <div class=\"group relative gap-8 drop-shadow-md  bg-white shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)] dark:bg-neutral-700\">
                            <div class=\"min-h-80 aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-md bg-gray-200 lg:aspect-none group-hover:opacity-75 lg:h-80\">
                                <img src=\"{{ app_pictures_dir }}/{{ product.picture }}\" alt=\"{{ product.label }}\" class=\"h-full w-full object-cover object-center lg:h-full lg:w-full\">
                            </div>
                            <div class=\"mt-4 flex justify-between p-4 \">
                                <div>

                                </div>
                                {% if product.productPromotion.promotionPercentage is defined %}
                                    <h3 class=\"text-md text-gray-900 font-bold p-4 \">
                                            {{ product.label }}
                                    </h3>
                                    {% set promoPrice = product.price * product.productPromotion.promotionPercentage  / 100 %}
                                    <p class=\"m-2 text-md text-grey-900 font-bold line-through\">{{ product.price }} €</p>
                                <span class=\"inline-flex items-center bg-red-100 text-red-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded-full dark:bg-red-900 dark:text-red-300\">
                                    <p class=\"m-2 text-md text-red-500 font-bold\">{{ product.price - promoPrice }} €</p>
                                </span>
                                {% else %}
                                <h3 class=\"text-md text-gray-900 font-bold p-4 \">
                                        {{ product.label }}
                                </h3>
                                <span class=\"inline-flex items-center bg-blue-100 text-blue-500 text-xs font-medium mr-2 px-2.5 py-0.5 rounded-full dark:bg-red-900 dark:text-red-300\">
                                    <p class=\"m-2 text-md text-blue-500\">{{ product.price }} €</p>
                                </span>
                                {% endif %}

                            </div>
                            <div class=\"block max-w-sm p-6 \">
                                <p class=\"font-normal text-gray-700 dark:text-gray-400\">{{ product.description | raw }}</p>
                            </div>


                        </div>
                    {% endfor %}
                </div>
            </div>
        </div>
    </div>
{% endblock %}
", "product/index.html.twig", "/Users/ben/Downloads/Projet-Mercadona-main/templates/product/index.html.twig");
    }
}

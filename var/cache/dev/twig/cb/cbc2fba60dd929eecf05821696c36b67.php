<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/my-dashboard.html.twig */
class __TwigTemplate_7f9226c29b3cc95f7c99fd15a35ef786 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/my-dashboard.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/my-dashboard.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\">
    <title>My Dashboard</title>
    <!-- Ajoutez ici vos liens vers les fichiers CSS -->
    <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/easyadmin@latest/easyadmin.min.css\">
</head>
<body>
<header>
    <!-- Ajoutez ici le contenu de votre en-tête -->
    <h1>Mon Tableau de bord EasyAdmin</h1>
</header>
<nav>
    <!-- Ajoutez ici votre menu de navigation -->
    <ul>
        <li><a href=\"";
        // line 17
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_category");
        echo "\">Accueil</a></li>

    </ul>
</nav>
<main>
    <!-- Ajoutez ici le contenu principal de votre tableau de bord -->
    <h2>Bienvenue sur votre tableau de bord EasyAdmin</h2>
    <!-- Inclure ici le rendu de EasyAdmin -->

</main>
<footer>
    <!-- Ajoutez ici le contenu de votre pied de page -->
    <p>© 2023 Mon Entreprise. Tous droits réservés.</p>
</footer>
<!-- Ajoutez ici vos liens vers les fichiers JavaScript -->
<script src=\"https://cdn.jsdelivr.net/npm/easyadmin@latest/easyadmin.min.js\"></script>
</body>
</html>
";
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "admin/my-dashboard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 17,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\">
    <title>My Dashboard</title>
    <!-- Ajoutez ici vos liens vers les fichiers CSS -->
    <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/easyadmin@latest/easyadmin.min.css\">
</head>
<body>
<header>
    <!-- Ajoutez ici le contenu de votre en-tête -->
    <h1>Mon Tableau de bord EasyAdmin</h1>
</header>
<nav>
    <!-- Ajoutez ici votre menu de navigation -->
    <ul>
        <li><a href=\"{{ path('admin_category') }}\">Accueil</a></li>

    </ul>
</nav>
<main>
    <!-- Ajoutez ici le contenu principal de votre tableau de bord -->
    <h2>Bienvenue sur votre tableau de bord EasyAdmin</h2>
    <!-- Inclure ici le rendu de EasyAdmin -->

</main>
<footer>
    <!-- Ajoutez ici le contenu de votre pied de page -->
    <p>© 2023 Mon Entreprise. Tous droits réservés.</p>
</footer>
<!-- Ajoutez ici vos liens vers les fichiers JavaScript -->
<script src=\"https://cdn.jsdelivr.net/npm/easyadmin@latest/easyadmin.min.js\"></script>
</body>
</html>
", "admin/my-dashboard.html.twig", "/Users/ben/Downloads/Projet-Mercadona-main/templates/admin/my-dashboard.html.twig");
    }
}

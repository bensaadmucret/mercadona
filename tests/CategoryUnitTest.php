<?php

namespace App\Tests;

use App\Entity\Category;
use App\Entity\Product;
use PHPUnit\Framework\TestCase;

class CategoryUnitTest extends TestCase
{
    public function testId(): void
    {
        $category = new Category();
        $this->assertNull($category->getId());
    }

    public function testLabel(): void
    {
        $category = new Category();
        $category->setLabel('Test Category');
        $this->assertEquals('Test Category', $category->getLabel());
    }

    public function testProducts(): void
    {
        $category = new Category();
        $product1 = new Product();
        $product2 = new Product();
        $product3 = new Product();

        // Add products to the category
        $category->addProduct($product1);
        $category->addProduct($product2);

        $this->assertCount(2, $category->getProducts());
        $this->assertTrue($category->getProducts()->contains($product1));
        $this->assertTrue($category->getProducts()->contains($product2));
        $this->assertFalse($category->getProducts()->contains($product3));

    }

    public function testToString(): void
    {
        $category = new Category();
        $category->setLabel('Test Category');
        $this->assertEquals('Test Category', (string)$category);
    }
}

<?php

declare(strict_types=1);

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class ResetPasswordUnitTest extends TestCase
{
    public function testSetEmail()
    {
        $user = new User();
        $email = 'test@test.com';
        $user -> setEmail($email);

        $this -> assertEquals($email, $user -> getEmail());
    }

    public function testGetRoles()
    {
        $user = new User();
        $roles = ['ROLE_USER'];
        $user -> setRoles($roles);

        $this -> assertEquals($roles, $user -> getRoles());
    }


}

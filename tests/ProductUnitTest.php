<?php

namespace App\Tests;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\Promotion;
use PHPUnit\Framework\TestCase;

class ProductUnitTest extends TestCase
{
    protected function setUp(): void
    {
        parent ::setUp();

        $this -> product = new Product();
        $this -> category = new Category();
        $this -> promotion = new Promotion();
        $this -> product -> setProductCategory($this -> category);
    }

    public function testGetterAndSetter()
    {
        $this -> product -> setLabel('Test product');
        $this -> assertEquals('Test product', $this -> product -> getLabel());

        $this -> product -> setDescription('This is a test product');
        $this -> assertEquals('This is a test product', $this -> product -> getDescription());

        $this -> product -> setPrice(9.99);
        $this -> assertEquals(9.99, $this -> product -> getPrice());

        $this -> product -> setPicture('test.jpg');
        $this -> assertEquals('test.jpg', $this -> product -> getPicture());

        $this -> assertEquals($this -> category, $this -> product -> getProductCategory());

        $this -> product -> setProductPromotion($this -> promotion);
        $this -> assertEquals($this -> promotion, $this -> product -> getProductPromotion());
    }

    public function testIsTrue()
    {
        $this -> product -> setLabel('Test product');
        $this -> product -> setDescription('This is a test product');
        $this -> product -> setPrice(9.99);
        $this -> product -> setPicture('test.jpg');

        $category = new Category();
        $promotion = new Promotion();

        $this -> product -> setProductCategory($category);
        $this -> product -> setProductPromotion($promotion);

        $this -> assertTrue($this -> product -> getLabel() === 'Test product');
        $this -> assertTrue($this -> product -> getDescription() === 'This is a test product');
        $this -> assertTrue($this -> product -> getPrice() === 9.99);
        $this -> assertTrue($this -> product -> getPicture() === 'test.jpg');
        $this -> assertTrue($this -> product -> getProductCategory() === $category);
        $this -> assertTrue($this -> product -> getProductPromotion() === $promotion);
    }

}
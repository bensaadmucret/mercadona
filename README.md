
# Mercadona's app

Bienvenue sur l'application en ligne de Mercadona. Cette plateforme vous permettra de consulter les produits de notre enseigne ainsi que les promotions en cours. L'ajout de nouveaux produits, de catégories et de promotions est géré via un espace administrateur sécurisé accessible après une authentification réussie.



## Pré-requis
Avant de commencer l'installation de l'application, assurez-vous que votre système dispose des outils suivants :
- PHP Php 8.1.0
- Symfony 6.2.8


## Téléchargement de l'application
Téléchargez l'application depuis ce dépôt Git.



## Installation des dépendances
Une fois que vous avez téléchargé le code source, vous devez installer les dépendances en utilisant Composer. Allez dans le répertoire de l'application et exécutez la commande suivante :
```composer install```

## Configuration de la base de données
Pour configurer la base de données, vous devez modifier le fichier .env et définir les informations de connexion à la base de données. Par exemple, si vous utilisez MySQL, vous devez définir les informations suivantes :
```
DATABASE_URL=mysql://db_user:db_password@db_host:db_port/db_name
```

Si vous utilisez SQLite, vous devez définir les informations suivantes :
```
DATABASE_URL=sqlite:///%kernel.project_dir%/var/app.db
```

enfin, si vous utilisez PostgreSQL, vous devez définir les informations suivantes :
```
DATABASE_URL=postgresql://db_user:db_password@db_host:db_port/db_name
```

## Création de la base de données
Une fois que vous avez configuré la base de données, vous devez créer la base de données en exécutant la commande suivante :

```
php bin/console doctrine:database:create
php bin/console doctrine:schema:create
php bin/console make:migration
php bin/console doctrine:migrations:migrate
php bin/console doctrine:fixtures:load

```

## Exécution des migrations
Les migrations sont des scripts qui permettent de mettre à jour la base de données en fonction des modifications apportées à l'application. Pour exécuter les migrations, utilisez la commande suivante :
``php bin/console doctrine:migrations:migrate`
## Lancement de l'application
Ìnstaller yarn :
```yarn install```
puis le mettre en route avec :
```yarn build```

Si vous preferer utiliser npm :
```npm install```
puis le mettre en route avec :
```npm run build```

Si vous utilisez le serveur Web intégré de Symfony, vous pouvez lancer l'application en exécutant la commande suivante :
```php bin/console server:run```

Mais si vous avez un installer la CLI de Symfony, vous pouvez lancer l'application en exécutant la commande suivante :
```symfony serve```

## Accès à l'application
## Création du premier administrateur
Pour créer le premier administrateur l'ajouter dans la base de données, avec le rôle :

["ROLE_ADMIN"]. Il sera alors possible d’accéder à l’espace d’administration avec ce compte.

